# Mangabot:

## Project Features:
 - Repost prevention
 - Keeps track of original poster
 - searches manga based on keywords, tags
 - import of mangas out of the whatsapp group
 - Export of mangas
 - manga lists
 - manga tags
 
## Features: 

 - [ ] Repost prevention
 - [ ] Manga search
 - [ ] Manga import
 - [ ] Manga list export
 - [ ] Manga Tags
 - [ ] Benutzerverwaltung

